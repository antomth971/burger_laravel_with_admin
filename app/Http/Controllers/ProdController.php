<?php

namespace App\Http\Controllers;

use App\Models\Produit;
use Illuminate\Http\Request;

class ProdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("pages.listeProduits", ["produits" => Produit::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("pages.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nom'  => 'required',
            'prix' => 'required',
            'description'  => 'required',
            'image' => 'required',
            'idCat' => 'required',
        ]);

        $produit = new Produit();
        $produit->nom = $request->input('nom');
        $produit->prix = $request->input('prix');
        $produit->description = $request->input('description');
        $produit->idCat = $request->input('idCat');

        if ($request->hasFile("image")) {
            //recupère le nom du fichier avec son extension
            $fileNameWithExt = $request->file("image")->getClientOriginalName();
            //recupère le nom du fichier
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            //recupère l’extension
            $extension = $request->file('image')->getClientOriginalExtension();
            //recupère le nom du fichier à stocker dans la BDD
            $fileNameToStore = $fileName . $extension;
            //upload du fichier dans le dossier Images
            $request->image->move(public_path('images'), $fileNameToStore);
        }
        $produit->image = $fileNameToStore;

        $produit->save();
        return redirect('produits/create')->with('message', 'Le produit ' . $produit->nom . ' a été ajouté');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produits = Produit::where('id', '=', $id)->get();
        return view("pages.listeDetail", ["produits" => $produits]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produit = Produit::find($id);
        return view('pages.update')->with('produit', $produit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $produit = Produit::find($id);
        $produit->nom = $request->input('nom');
        $produit->description = $request->input('description');
        $produit->prix = $request->input('prix');
        $produit->idCat = $request->input('idCat');

        $fileNameWithExt = $request->file("image")->getClientOriginalName();
        $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
        $extension = $request->file('image')->getClientOriginalExtension();
        $fileNameToStore = $fileName . '.' . $extension;

        $request->image->move(public_path('images'), $fileNameToStore);

        $produit->image = $fileNameToStore;

        $produit->update();
        return redirect("produits/$produit->id")->with('message', 'Le produit ' . $produit->nom . ' a été modifié');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produit = Produit::find($id);
        $produit->delete();
        return redirect('produits')->with('message', 'Le produit ' . $produit->nom . ' a été supprimé');
    }
}
