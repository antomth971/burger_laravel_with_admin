<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Produit
 * 
 * @property int $id
 * @property string $nom
 * @property string $description
 * @property float $prix
 * @property string $image
 * @property int $idCat
 * 
 * @property Category $category
 *
 * @package App\Models
 */
class Produit extends Model
{
	protected $table = 'produits';
	public $timestamps = false;

	protected $casts = [
		'prix' => 'float',
		'idCat' => 'int'
	];

	protected $fillable = [
		'nom',
		'description',
		'prix',
		'image',
		'idCat'
	];

	public function category()
	{
		return $this->belongsTo(Category::class, 'idCat');
	}
}
