<?php

use App\Http\Controllers\ProdController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('produits', ProdController::class)->middleware('admin');

Auth::routes();

Route::get('accueil', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/', function () {
    return view("pages.accueilNull");
});
