@extends('presentation.app')
@extends('presentation.nav')


@section('contenu')
    <center>
        <h1>Bonjour {{ Auth::user()->name }}</h1>
        <br>
        <p>Bonjour, et bienvenue sur mon site. <br>Une barre de naviguation est disponible si vous souhaiter vous orienter.
        </p>
    </center>
@endsection
