@extends('presentation.app')
@extends('presentation.nav')

@section('contenu')
    <center>
        <h1>Liste des Produits</h1>
    </center>
    @if (Session::has('message'))
        <div class="alert alert-succes">
            {{ Session::get('message') }}
        </div>
    @endif

    <div class="container">
        <table class="table" border="1">
            <thead>
                <tr>

                    <th>Nom du produit</th>
                    <th>Détails</th>
                </tr>
            </thead>
            @foreach ($produits as $item)
                <tbody>
                    <tr>
                        <td>{{ $item->nom }}</td>
                        <td><a href="{{ URL::to('produits/' . $item->id) }}" class="btn btn-primary">Détails</a></td>
            @endforeach
            </tr>
            </tbody>
        </table>







    </div>
@endsection
